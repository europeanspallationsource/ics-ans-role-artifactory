ics-ans-role-artifactory
===================

Ansible role to install artifactory.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-artifactory
```

License
-------

BSD 2-clause

import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_for_git_installed(Package):
    assert Package("git").is_installed


def test_ssl_cert_created(File):
    assert File("/data/nginx/ssl/example.pem").exists


def test_ssl_cert_key_created(File):
    assert File("/data/nginx/ssl/example.key").exists


def test_artifactory_containers(Command, Sudo):
    with Sudo():
        cmd = Command('docker ps')
    assert cmd.rc == 0
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == [u'artifactory', u'nginx', u'postgresql']
